import sys
sys.path.insert(0, "..")
import time


from opcua import ua, Server


if __name__ == "__main__":

    # setup our server
    server = Server()
    server.set_endpoint("opc.tcp://0.0.0.0:4842/freeopcua/server/")

    # setup our own namespace, not really necessary but should as spec
    uri = "http://examples.freeopcua.github.io"

    idx = server.register_namespace(uri)
    server.start()
    print idx
    # get Objects node, this is where we should put our nodes
    objects = server.get_objects_node()

    # populating our address space
    myobj = objects.add_object(idx, "MyObject")
    myvar = myobj.add_variable(idx, "MyVariable", 6.7)
    myvar.set_writable()    # Set MyVariable to be writable by clients
    myvar1 = myobj.add_variable(idx, "MyVariable_1", "hello")
    myvar1.set_writable()
    print "Number of variables: ", len(myobj.get_variables())
    # starting!

    
    try:
        count = 0
        while True:
            time.sleep(1)
            count += 0.1
            print count
            myvar.set_value(count)
            myvar1.set_value("hello" + str(count))
            myvar.get_value()
    finally:
        #close connection, remove subcsriptions, etc
        server.stop()
