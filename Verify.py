import csv, os


class Verify(object):

  def __init__(self):
    pass

  def dataIntegrityServers(self,data):
    self.data = data
    check = 0
    errors_str = ""
    iName = 0
    iDescription = 1
    iEndPoint = 2
    iUri = 3
    for row in range(len(self.data)):
      aserverName = self.data[row][iName]
      aserverEndPoint = self.data[row][iEndPoint]
      aserverUri = self.data[row][iUri]
      for otherrow in range(row + 1, len(self.data)):
        otherServerName = self.data[otherrow][iName]
        otherserverEndPoint = self.data[otherrow][iEndPoint]
        otherserverUri = self.data[otherrow][iUri]
        if aserverName == otherServerName:
          errors_str = errors_str + ("Duplicated Server Name: " + aserverName + "\n" + " Line: " + str(row) + " and Line: "+str(otherrow)+"\n")
          check = 1

        if aserverEndPoint == otherserverEndPoint:
          errors_str = errors_str + ("Duplicated Endpoint Name: " + aserverEndPoint + "\n" + "Line: " + str(row) + " and Line: "+str(otherrow)+"\n")
          check = 1

        if aserverUri == otherserverUri:
          errors_str = errors_str + ("Duplicated Uri Name: " + aserverUri + "\n" + "Line: " + str(row) + " and Line: "+str(otherrow)+"\n")
          check = 1

    return check, errors_str


  def dataIntegrityUnits(self,data):
    self.data = data
    check = 0
    errors_str = ""

    iNodeId = 0
    iInServer = 1
    iName = 2
    iDescription = 3

    for row in range(len(self.data)):
      aUnitName = self.data[row][iName]
      anodeId = self.data[row][iNodeId]
      aInserver = self.data[row][iInServer]
      for otherrow in range(row+1, len(self.data)):
          otherUnitName = self.data[otherrow][iName]
          othernodeId = self.data[otherrow][iNodeId]
          otherInserver = self.data[otherrow][iInServer]
          if aInserver + aUnitName == otherInserver + otherUnitName:
            errors_str = errors_str + ("Duplicated unit name: " + otherUnitName + "\n" + "Line: " + str(row) + " and Line: "+str(otherrow)+"\n")
            check = 1

          if anodeId == othernodeId:
            errors_str = errors_str + ("Duplicated Node ID: " + anodeId + "\n" + "Line: " + str(row) + " and Line: "+str(otherrow)+"\n")
            check = 1

    return check, errors_str


  def dataIntegrityChannels(self,data):

    self.data = data

    check = 0
    errors_str = ""

    for row in range(len(self.data)):
      cvalue = self.data[row][2]
      cunit = self.data[row][1]
      for row1 in range(row+1, len(self.data)):
          if cvalue == self.data[row1][2] and cunit == self.data[row1][1]:
            errors_str = errors_str + ("Duplicated channel name: " + cvalue + "\n" + "Line: " + str(row1+1)+ "\n")
            check = 1



    return check,errors_str
