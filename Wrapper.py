

from threading import Thread
import logging
import logging.handlers
from LastLogsHandler import *
from Bsetpoint import *
from Server import *
import Verify as vf


class Wrapper(Thread):

    log = logging.getLogger('Wrapper')
    log.setLevel(logging.DEBUG)


    hdlr = logging.handlers.RotatingFileHandler('logs.log', mode='a', maxBytes=1024*5*1000, backupCount=3, encoding=None, delay=False)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s \t\t %(message)s')
    hdlr.setFormatter(formatter)
    hdlr.setLevel(logging.DEBUG)
    log.addHandler(hdlr)


    ch = logging.StreamHandler()
    ch.setFormatter(formatter)
    ch.setLevel(logging.INFO)
    log.addHandler(ch)

    # GUI log
    logHandler = LastLogsHandler(500)
    logHandler.setFormatter(formatter)
    logHandler.setLevel(logging.INFO)
    log.addHandler(logHandler)

    def __init__(self):
        Thread.__init__(self)
        self.servers = []
        self.runFlag=False
        self.setDaemon(True)
        self.period=1
        self.toVerifyChannels = 0
        self.listOfChannelsSpreads = []


        Wrapper.log.info("Wrapper created.")

    def addServer(self, name, description, endpoint, uri):
        server = Server(name, description, endpoint, uri)
        self.servers.append(server)
        return server


    def readTxt(self,file):

        dataServer = []
        dataUnit = []
        dataChannel = []

        with open(file) as textFile:
            for line in textFile:
                if line[0:2] == "/s":
                    lines = line.rstrip('\r\n').split('\t')
                    dataServer.append(lines[1:])
                if line[0:2] == "/u":
                    lines = line.rstrip('\r\n').split('\t')
                    dataUnit.append(lines[1:])
                if line[0:2] == "/c":
                    lines = line.rstrip('\r\n').split('\t')
                    dataChannel.append(lines[1:])

        sheet_Server = dataServer[0:]
        sheet_units = dataUnit[0:]
        sheet_channel = dataChannel[0:]
        #print sheet_units
        #print sheet_channel

      # Verificar erros!

        self.toVerifyChannels = sheet_channel
        self.toVerifyUnits = sheet_units
        self.toVerifyServers = sheet_Server

        a = vf.Verify()

        tupleCheckChannels = a.dataIntegrityChannels(self.toVerifyChannels)
        tupleCheckUnits = a.dataIntegrityUnits(self.toVerifyUnits)
        tupleCheckServer = a.dataIntegrityServers(self.toVerifyServers)


        self.checkChannels = 0
        self.checkUnits = 0
        self.checkServer = 0

        if tupleCheckChannels[0] == 1:
            self.checkChannels = 1

        if tupleCheckServer[0] == 1:
            self.checkServer = 1

        if tupleCheckServer[0] == 0 and tupleCheckUnits[0] == 0 and tupleCheckChannels[0] == 0:
            self.toVerifyChannels = 0
            self.toVerifyUnits = 0
            self.toVerifyServers = 0

            self.importArrays(sheet_Server, sheet_units, sheet_channel)

        else:
            self.toVerifyChannels = tupleCheckChannels[1]
            self.toVerifyUnits = tupleCheckUnits[1]
            self.toVerifyServers = tupleCheckServer[1]
            print self.toVerifyChannels
            print self.toVerifyUnits
            print self.toVerifyServers


    def importArrays(self,servers, units, variables):

        #print servers
        #print units
        #print variables

        s_name = 0
        s_description = 1
        s_endpoint = 2
        s_uri = 3

        u_nodeid = 0
        u_toserver = 1
        u_name = 2
        u_description = 2

        v_id = 0
        v_unitId = 1
        v_name = 2
        v_description = 3
        v_behavior = 4
        v_command = 5

        for server in servers:
            server_name = server[s_name]
            server_description = server[s_description]
            server_endpoint = server[s_endpoint]
            server_uri = server[s_uri]
            if server_name != "" and server_endpoint != "" and server_uri != "":
                serverObj = self.addServer(server_name,server_description,server_endpoint,server_uri)

                for unit in units:

                    unit_id = unit[u_nodeid]
                    unit_server = unit[u_toserver]
                    unit_name = unit[u_name]
                    unit_description = unit[u_description]

                    if unit_id != "" and unit_server != "" and unit_name != "" and unit_server == serverObj.name:

                        unitObj = serverObj.addAddressSpace(unit_name,unit_id,unit_description)


                        for var in variables:
                            #print var
                            var_id = int(var[v_id])
                            var_unidID = int(var[v_unitId])
                            var_name = var[v_name]
                            var_description = var[v_description]
                            var_behavior = var[v_behavior]

                            behavior = None
                            if var_id != "" and var_unidID != "" and var_name != "" and var_behavior != "" and unitObj.nodeID == var_unidID:
                                newvar = unitObj.addVariable(var_unidID,var_name, 0, var_behavior)

                                if var_behavior.lower() == "bsetpoint":
                                    behavior = Bsetpoint(5, 1, 0, newvar)

                                elif var_behavior.lower() == "client":
                                    var_command = var[v_command]
                                    behavior = Bclient(var_command)

                                newvar.behavior = behavior


    def setPeriod(self, period):
        self.period = period

    def getLogString(self):
        return self.logHandler.getStr()

    def run(self):
        self.runFlag = True
        while self.runFlag:
            self.runCycle()
            time.sleep(self.period)


    def stop(self):
        self.runFlag = False

    def restart(self):
        self.runFlag = True

    def runCycle(self):

        for s in self.servers:
            for n in s.aspaces:
                for c in n.variables:
                    c.updateValue()






            


