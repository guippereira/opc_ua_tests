import logging

class LastLogsHandler(logging.Handler):
    def __init__(self,size):
        logging.Handler.__init__(self)
        self.strA = []
        self.i = 0
        self.len = size
        for i in range(self.len):
            self.strA.append(None)

    def emit(self, record):
        self.strA[self.i] = record
        self.i+=1
        if self.i == self.len:
            self.i = 0

    def getStr(self):
        s=""
        for n in range(self.len):
            k = self.i - n-1
            if k < 0:
                k = k + self.len
            if self.strA[k] != None:
                s+= self.format(self.strA[k])+"\r\n"
        return s