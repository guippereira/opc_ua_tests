import logging
from LastLogsHandler import *

class Variable(object):
    log = logging.getLogger('Wrapper.Variable')

    def __init__(self, addressSpace, name, idx, value, behavior, writable=True):

        self.addressSpace = addressSpace
        self.name = name
        self.idx = idx
        self.value = value
        self.writable = writable
        self.behavior = behavior
        self.myvar = self.addressSpace.addressSpace.add_variable(idx, name, value)


        if self.writable:
            self.myvar.set_writable()  # Set MyVariable to be writable by clients

        self.log = logging.getLogger('wrapper.variable.' + self.name)
        self.logHandler = LastLogsHandler(50)
        self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s \t\t %(message)s')
        self.logHandler.setFormatter(self.formatter)
        self.logHandler.setLevel(logging.INFO)
        self.log.addHandler(self.logHandler)
        self.log.debug("Variable created")


    def updateValue(self):

        newValue = self.myvar.get_value()

        if newValue != self.value:
            print "Client Wrote to the channel:", self.name
            print newValue
            self.value = newValue
            self.behavior.setSetpoint(newValue)

        bvalue = self.behavior.getValue()
        self.myvar.set_value(bvalue)
        self.value = bvalue


    def getLogString(self):
        return self.logHandler.getStr()

    def __str__(self):
        return "Channel: %s %s" % (self.name, self.behavior)




