from Variable import *
from Bsetpoint import *
from BcreateChannels import *
class AddressSpace(object):
    log = logging.getLogger('wrapper.AddressSpace')

    def __init__(self, server, name, nodeid, description):

        self.name = name
        self.description = description
        self.variables = []
        self.server = server
        self.nodeID = int(nodeid)

        objects = self.server.opcuaServer.get_objects_node()
        self.addressSpace = objects.add_object(self.nodeID, self.name)

        self.var_newname = 0
        self.var_newvalue = 0
        self.var_newbehavior = 0
        self.var_newaction = 0
        self.toCreateChannelsInputs()

    def addVariable(self, idx, name, value, behavior, writable=True):

        var = Variable(self, name, int(idx), value, behavior, writable)
        self.variables.append(var)
        return var


    def toCreateChannelsInputs(self):

        self.var_newaction = self.addVariable(self.nodeID,"Action",0,"")
        behavior = BcreateChannel(self.var_newaction)
        self.var_newaction.behavior = behavior

        self.var_newname = self.addVariable(self.nodeID, "New Name", "", "")
        behavior = BcreateChannel(self.var_newname)
        self.var_newname.behavior = behavior

        self.var_newvalue = self.addVariable(self.nodeID, "New Value", 0, "")
        behavior = BcreateChannel(self.var_newvalue)
        self.var_newvalue.behavior = behavior

        self.var_newbehavior = self.addVariable(self.nodeID, "New Behavior", "", "")
        behavior = BcreateChannel(self.var_newbehavior)
        self.var_newbehavior.behavior = behavior

    def __str__(self):
        return ("Unit: %s %s" %(self.name, self.description))
