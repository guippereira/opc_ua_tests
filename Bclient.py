from Behavior import *
import numpy

class Bsetpoint(Behavior):

    def __init__(self,command):

        self.command = command


    def getValue(self):

        self.value = self.command

        return self.value

    def setSetpoint(self,setpoint):

        self.setpoint=setpoint

    def __str__(self):
        return "Bsetpoint"