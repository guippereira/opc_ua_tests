


from abc import ABCMeta, abstractmethod

class Behavior:
    __metaclass__ = ABCMeta

    def __init__(self, name, description, tcp_sim, tcp_real, ip_real):
        self.value = 0


    @abstractmethod
    def getValue(self):
        pass