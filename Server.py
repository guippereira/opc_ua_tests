import opcua
import time
from AddressSpace import *


class Server(object):
    log = logging.getLogger('simulator.Server')

    def __init__(self, name, description, endpoint, uri):

        self.name = str(name)
        self.description = description

        self.opcuaServer = opcua.Server()
        self.opcuaServer.set_endpoint(endpoint)
        self.opcuaServer.set_server_name(name)
        self.id = self.opcuaServer.register_namespace(uri)

        self.aspaces = []
        self.opcuaServer.start()

    def addAddressSpace(self, name, nodeid, description):
        Aspace = AddressSpace(self, name, nodeid, description)

        self.aspaces.append(Aspace)

        return Aspace



    def startServer(self):

        self.opcuaServer.start()

        try:
            count = 0
            while True:
                for adsp in self.aspaces:
                    if isinstance(adsp,AddressSpace):
                        for var in adsp.variables:
                            if isinstance(var,Variable):
                                var.updateValue()
                time.sleep(1)

        finally:
            # close connection, remove subcsriptions, etc
            self.opcuaServer.stop()


    def getAddressSpace(self):

        return self.aspaces




    def __str__(self):
        return ("Server: %s %s" %(self.name, self.description))