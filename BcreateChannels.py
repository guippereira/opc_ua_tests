from Behavior import *
import numpy

class BcreateChannel(Behavior):

    def __init__(self,variable):


        self.variable = variable


        self.setpoint = 0


    def getValue(self):
        try:
            self.value = self.setpoint #+ self.A * numpy.random.normal(0, self.sigma)
        except Exception, e:
            self.value = self.setpoint
        return self.value

    def setSetpoint(self,setpoint):

        self.setpoint=setpoint

        if self.setpoint == 10 and self.variable.name == "Action":

            self.createChannel()


    def createChannel(self):

        idx = self.variable.addressSpace.nodeID
        name = self.variable.addressSpace.var_newname
        value = self.variable.addressSpace.var_newvalue
        behavior = self.variable.addressSpace.var_newbehavior
        unit = self.variable.addressSpace.addVariable(idx, name, value, behavior, writable=True)



    def __str__(self):
        return "Bsetpoint"