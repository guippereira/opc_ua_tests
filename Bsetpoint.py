from Behavior import *
import numpy

class Bsetpoint(Behavior):

    def __init__(self,A,sigma,setpoint,variable):
        self.A = float(A)
        self.sigma = float(sigma)
        self.variable = variable


        self.setpoint = setpoint


    def getValue(self):
        try:
            self.value = self.setpoint + self.A * numpy.random.normal(0, self.sigma)
        except Exception, e:
            self.value = self.setpoint
        return self.value

    def setSetpoint(self,setpoint):

        self.setpoint=setpoint

    def __str__(self):
        return "Bsetpoint"